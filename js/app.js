window.onload = function(){

  form1.onsubmit=function()
  { 
    var query = document.getElementById("query").value;
    var columns = get_columns();
    var column = unite_columns(columns);
    var queries = make_queries(query,column);
    document.getElementById('result').value = queries.join('\n');

    return false;
  };
};

function make_queries(query,column) //templating the columns into the placeholder
{
  var n = (query.split("{}").length - 1) ;
  var queries = [];
  for (var i=0; i< column.length; i++)   
  {
     queries[i] = query;
  }

  for (var i=0; i<n; i++)
  {
    for (var ii=0; ii< column.length; ii++)   
    {
       queries[ii] = queries[ii].replace("{}",column[ii][i]);
    }
  }
  return queries;
}


function get_columns() //getting the filled columes and send them back as array
{
  var c = [];
  var columns = document.getElementById('columns');
  var e = columns.getElementsByTagName('textarea');
  for (var i=0; i<e.length; i++) 
  {
    var a = e[i].value.match(/[^\r\n]+/g);
    if(a)
    {
      c[i] = a;
    }
  }
  return c;
}


function unite_columns(c) //take the smallest column and create one master column with other columns as nested array 
{
  var rows = c[0].length;
  for(var i=0; i<c.length; i++) 
  {
    if(c[i].length < rows) //get colum with the least rows
    {
      rows = c[i].length;
    }
  }

  var columns = [];
  for(var i=0; i < rows; i++) 
  {
    columns[i] = [];
  }

  for(var i=0; i<c.length; i++) 
  {
    for(var ii=0; ii < c[i].length; ii++) 
    {
      if(ii < rows)
      {
        columns[ii].push(c[i][ii]);
      }
    }     
  }
  return columns;
}
